package org.luis.security.recorder.video;

import java.util.ArrayList;
import java.util.List;
import org.luis.security.recorder.ui.util.ThreadUtil;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.luis.security.recorder.main.Logger;

/**
 *
 * @author luis
 */
public abstract class Worker implements Logger {

	private static final Queue<Runnable> staticActions = new ConcurrentLinkedQueue<>();

	boolean running = false;
	boolean isThreadRunning = false;
	boolean fullyStarted;
	long forceStopAfter = 1500l;

	private String name;

	private Thread workerThread;
	private long sleepBetween = 100;

	static {
		new Thread(new StaticActionsThread(), "vm-sync-actions").start();
	}

	public Worker(String name) {
		this(name, 100);
	}

	public Worker(String name, long sleepTime) {
		this(name, sleepTime, 1500l);
	}

	public Worker(String name, long sleepTime, long forceStopAfter) {
		this.name = name + "-" + ((int) (new Random().nextFloat() * 100f));
		this.sleepBetween = sleepTime;
		this.forceStopAfter = forceStopAfter;
	}

	public String getName() {
		return name;
	}

	public void start() {
		start(false);
	}

	public void start(boolean wait) {
		// Stop current thread
		stop();

		log("start");
		running = true;
		fullyStarted = false;
		workerThread = new Thread(new WorkerThread(), name);
		workerThread.start();
		if (wait) {
			do {
				ThreadUtil.sleep(5l);
			} while (!fullyStarted);
		}
	}

	public void stop() {
		stop(false);
	}

	public void stop(boolean wait) {

		if (!running) {
			return;
		}

		log("stop");
		this.running = false;
		Runnable stopAction = () -> {
			if (isThreadRunning) {
				if (forceStopAfter > 0) {
					boolean timedout = ThreadUtil.waitTo(forceStopAfter, () -> isThreadRunning);
					if (!timedout) {
						forceStop();
					}
				}
			}
		};

		if (wait) {
			stopAction.run();
		} else {
			synchronizedAction(stopAction);
		}
	}

	abstract void doJob() throws Exception;

	abstract void onStop() throws Exception;

	abstract void onStart() throws Exception;

	static void synchronizedAction(Runnable action) {
		synchronized (staticActions) {
			staticActions.offer(action);
		}
	}

	private static void executeSaticActions() {
		List<Runnable> actions = new ArrayList<>();
		synchronized (staticActions) {
			actions.addAll(staticActions);
			staticActions.clear();
		}

		actions.forEach((a) -> {
			try {
				a.run();
			} catch (Exception e) {
			}
		});
	}

	public static void waitForExit() {
		executeSaticActions();
	}

	public boolean isRunning() {
		return running;
	}

	private void forceStop() {
		log("forceStop");
		try {
			workerThread.interrupt();
		} catch (Throwable e) {
		}

		isThreadRunning = false;

	}

	private static class StaticActionsThread implements Runnable {

		@Override
		public void run() {
			while (true) {
				executeSaticActions();
				ThreadUtil.sleep(100);
			}
		}

	}

	private class WorkerThread implements Runnable {

		@Override
		public void run() {
			boolean success = false;
			isThreadRunning = true;

			try {
				onStart();
				success = true;
			} catch (Exception e) {
				error(e);
			}

			log("started? " + success);
			if (!success) {
				running = false;
			}
			fullyStarted = true;
			while (running) {
				try {
					doJob();
				} catch (Exception e) {
					error(e);
				}

				ThreadUtil.sleep(sleepBetween);
			}

			try {
				onStop();
			} catch (Exception e) {
				error(e);
			}

			isThreadRunning = false;
		}

	}
}
