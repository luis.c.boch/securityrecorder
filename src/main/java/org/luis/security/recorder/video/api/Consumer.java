package org.luis.security.recorder.video.api;

/**
 *
 * @author luis
 * @param <T>
 */
public interface Consumer<T> {

	Class<T> getType();

	void accept(T value);

	default boolean isReadyToReceive() {
		return true;
	}

}
