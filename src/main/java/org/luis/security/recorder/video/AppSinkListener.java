package org.luis.security.recorder.video;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.freedesktop.gstreamer.Buffer;
import org.freedesktop.gstreamer.FlowReturn;
import org.freedesktop.gstreamer.Sample;
import org.freedesktop.gstreamer.Structure;
import org.freedesktop.gstreamer.elements.AppSink;
import org.luis.security.recorder.video.api.ImageConsumer;
import org.luis.security.recorder.video.api.ImageData;

/**
 *
 * @author luis
 */
class AppSinkListener implements AppSink.NEW_SAMPLE, AppSink.NEW_PREROLL {

	final ImageConsumer consumer;

	public AppSinkListener(ImageConsumer consumer) {
		this.consumer = consumer;
	}

	public void rgbFrame(boolean isPrerollFrame, int width, int height, IntBuffer rgb) {

		// If there is already a swing update pending, also drop this frame.
		final BufferedImage renderImage = getBufferedImage(width, height);
		int[] pixels = ((DataBufferInt) renderImage.getRaster().getDataBuffer()).getData();
		rgb.get(pixels, 0, width * height);
		consumer.accept(new ImageData(renderImage, width, height, 0, 0));
	}

	BufferedImage getBufferedImage(int width, int height) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		image.setAccelerationPriority(0.0f);
		return image;
	}

	@Override
	public FlowReturn newSample(AppSink elem) {
		Sample sample = elem.pullSample();
		Structure capsStruct = sample.getCaps().getStructure(0);
		int w = capsStruct.getInteger("width");
		int h = capsStruct.getInteger("height");
		Buffer buffer = sample.getBuffer();
		ByteBuffer bb = buffer.map(false);
		if (bb != null) {
			rgbFrame(false, w, h, bb.asIntBuffer());
			buffer.unmap();
		}
		sample.dispose();
		return FlowReturn.OK;
	}

	@Override
	public FlowReturn newPreroll(AppSink elem) {
		Sample sample = elem.pullPreroll();
		Structure capsStruct = sample.getCaps().getStructure(0);
		int w = capsStruct.getInteger("width");
		int h = capsStruct.getInteger("height");
		Buffer buffer = sample.getBuffer();
		ByteBuffer bb = buffer.map(false);
		if (bb != null) {
			rgbFrame(false, w, h, bb.asIntBuffer());
			buffer.unmap();
		}
		sample.dispose();
		return FlowReturn.OK;
	}

}
