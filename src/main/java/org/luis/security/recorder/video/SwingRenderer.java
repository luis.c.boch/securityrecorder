/* 
 * Copyright (c) 2018 Neil C Smith
 * Copyright (c) 2007 Wayne Meissner
 * 
 * This file is part of gstreamer-java.
 *
 * This code is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License version 3 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with this work.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.luis.security.recorder.video;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.luis.security.recorder.ui.util.Observable;
import org.luis.security.recorder.video.api.ImageConsumer;
import org.luis.security.recorder.video.api.ImageData;

/**
 *
 */
public class SwingRenderer extends javax.swing.JComponent implements ImageConsumer {

	RenderComponent renderComponent = new RenderComponent();
	boolean keepAspect = true;
	BufferedImage currentImage;
	VolatileImage volatileImage;
	boolean useVolatile = false;
	Observable<State> state = new Observable<>();
	int renderFPS;

	long lastFrame;

	/**
	 * Creates a new instance of GstVideoComponent
	 *
	 * @param appsink
	 */
	public SwingRenderer() {
		// Don't use a layout manager - the output component will positioned within this 
		// component according to the aspect ratio and scaling mode
		//
		setLayout(null);
		add(renderComponent);

		//
		// Listen for the child changing its preferred size to the size of the 
		// video stream.
		//
		renderComponent.addPropertyChangeListener("preferredSize", new PropertyChangeListener() {

			public void propertyChange(PropertyChangeEvent evt) {
				setPreferredSize(renderComponent.getPreferredSize());
				scaleVideoOutput();
			}
		});
		//
		// Scale the video output in response to this component being resized
		//
		addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent arg0) {
				scaleVideoOutput();
			}

		});

		renderComponent.setBounds(getBounds());
		state.addListener((n, o) -> repaint());

		setOpaque(true);
		setBackground(Color.BLACK);
	}

	/**
	 * Scales the video output component according to its aspect ratio
	 */
	private void scaleVideoOutput() {
		final Component child = renderComponent;
		final Dimension childSize = child.getPreferredSize();
		final int width = getWidth(), height = getHeight();
		// Figure out the aspect ratio
		double aspect = keepAspect ? (double) childSize.width / (double) childSize.height : 1.0f;

		//
		// Now scale and position the videoChild component to be in the correct position
		// to keep the aspect ratio correct.
		//
		int scaledHeight = (int) ((double) width / aspect);
		if (!keepAspect) {
			//
			// Just make the child match the parent
			//
			child.setBounds(0, 0, width, height);
		} else if (scaledHeight < height) {
			//
			// Output window is taller than the image is when scaled, so move the 
			// video component to sit vertically in the centre of the VideoComponent.
			//
			final int y = (height - scaledHeight) / 2;
			child.setBounds(0, y, width, scaledHeight);
		} else {
			final int scaledWidth = (int) ((double) height * aspect);
			final int x = (width - scaledWidth) / 2;
			child.setBounds(x, 0, scaledWidth, height);
		}
	}

	public void setKeepAspect(boolean keepAspect) {
		this.keepAspect = keepAspect;
	}

	@Override
	public boolean isLightweight() {
		return true;
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (isOpaque()) {

			Graphics2D g2d = (Graphics2D) g.create();
			g2d.setColor(getBackground());
			g2d.fillRect(0, 0, getWidth(), getHeight());

			if (state.get() != null) {
				g2d.setColor(getColor(state.get()));
				g2d.fillRect(getSize().width - 40, 0, 60, 40);
			}

			g2d.dispose();
		}
	}

	private Color getColor(State state) {
		if (state == null) {
			return null;
		}

		if (State.UPDATED.equals(state)) {
			return Color.green;
		}

		if (State.WAITING.equals(state)) {
			return Color.yellow;
		}

		return Color.red;

	}

	private class RenderComponent extends javax.swing.JComponent {

		private static final long serialVersionUID = -4736605073704494268L;

		@Override
		protected void paintComponent(Graphics g) {
			int width = getWidth(), height = getHeight();
			Graphics2D g2d = (Graphics2D) g.create();
			g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
					RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			if (currentImage != null) {
				render(g2d, 0, 0, width, height);
			} else {
				g2d.setColor(getBackground());
				g2d.fillRect(0, 0, width, height);
			}
			g2d.dispose();
		}

		@Override
		public boolean isOpaque() {
			return SwingRenderer.this.isOpaque();
		}

		@Override
		public boolean isLightweight() {
			return true;
		}
	}

	private void renderVolatileImage(BufferedImage bufferedImage) {
		int w = bufferedImage.getWidth(), h = bufferedImage.getHeight();
		GraphicsConfiguration gc = getGraphicsConfiguration();
		if (volatileImage == null || volatileImage.getWidth() != w
				|| volatileImage.getHeight() != h
				|| volatileImage.validate(gc) == VolatileImage.IMAGE_INCOMPATIBLE) {
			if (volatileImage != null) {
				volatileImage.flush();
			}
			volatileImage = gc.createCompatibleVolatileImage(w, h);
			volatileImage.setAccelerationPriority(1.0f);
		}

		Graphics2D g = volatileImage.createGraphics();
		g.drawImage(bufferedImage, 0, 0, null);
		g.dispose();
	}

	/**
	 * Renders to a volatile image, and then paints that to the screen. This
	 * helps with scaling performance on accelerated surfaces (e.g. OpenGL)
	 *
	 * @param g the graphics to paint the image to
	 * @param x the left coordinate to start painting at.
	 * @param y the top coordinate to start painting at.
	 * @param w the width of the paint area
	 * @param h the height of the paint area
	 */
	private void volatileRender(Graphics g, int x, int y, int w, int h) {
		renderVolatileImage(currentImage);
		g.drawImage(volatileImage, x, y, w, h, null);
	}

	/**
	 * Renders directly to the given <tt>Graphics</tt>. This is only really
	 * useful on MacOS where swing graphics are unaccelerated so using a
	 * volatile just incurs an extra memcpy().
	 *
	 * @param g the graphics to paint the image to
	 * @param x the left coordinate to start painting at.
	 * @param y the top coordinate to start painting at.
	 * @param w the width of the paint area
	 * @param h the height of the paint area
	 */
	private void heapRender(Graphics g, int x, int y, int w, int h) {
		g.drawImage(currentImage, x, y, w, h, null);
	}

	/**
	 * Renders the current frame to the given <tt>Graphics</tt>.
	 *
	 * @param g the graphics to paint the image to
	 * @param x the left coordinate to start painting at.
	 * @param y the top coordinate to start painting at.
	 * @param w the width of the paint area
	 * @param h the height of the paint area
	 */
	private void render(Graphics g, int x, int y, int w, int h) {
		if (useVolatile) {
			volatileRender(g, x, y, w, h);
		} else {
			heapRender(g, x, y, w, h);
		}

	}

	private int width, height;

	@Override
	public void accept(ImageData image) {

		currentImage = image.getImage();

		if (image.getWidth() != this.width || image.getHeight() != this.height) {
			renderComponent.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		}

		if (renderComponent.isVisible()) {
			renderComponent.repaint();
		}

	}

}
