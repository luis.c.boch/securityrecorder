/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luis.security.recorder.video;

/**
 *
 * @author luis
 */
public enum State {
	/**
	 * Last image is updated
	 */
	UPDATED,
	/**
	 * Last image is 5 secs out of date
	 */
	WAITING,
	/**
	 * Last image is 15 secs out of date
	 */
	LOST;
}
