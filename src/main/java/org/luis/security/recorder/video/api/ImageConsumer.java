package org.luis.security.recorder.video.api;

/**
 *
 * @author luis
 */
public interface ImageConsumer extends Consumer<ImageData> {

	@Override
	void accept(ImageData image);
	
	@Override
	default Class<ImageData> getType() {
		return ImageData.class;
	}
}
