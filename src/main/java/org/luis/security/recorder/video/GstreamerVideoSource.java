package org.luis.security.recorder.video;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import org.freedesktop.gstreamer.Caps;
import org.freedesktop.gstreamer.Element;
import org.freedesktop.gstreamer.ElementFactory;
import org.freedesktop.gstreamer.Gst;
import org.freedesktop.gstreamer.elements.AppSink;
import org.freedesktop.gstreamer.elements.PlayBin;
import org.luis.security.recorder.main.VideoSource;
import org.luis.security.recorder.ui.util.Observable;
import org.luis.security.recorder.video.api.AudioConsumer;
import org.luis.security.recorder.video.api.ImageConsumer;
import org.luis.security.recorder.video.api.ImageData;
import org.luis.security.recorder.video.api.StreamSource;

/**
 *
 * @author luis
 */
public class GstreamerVideoSource implements StreamSource, org.luis.security.recorder.main.Logger {

	static {
		Gst.init();
	}

	final List<ImageConsumer> imageConsumers = new ArrayList<>();
	final List<AudioConsumer> audioConsumers = new ArrayList<>();

	final Observable<VideoSource> source;
	final AppSink videosink;
	PlayBin playbin;

	public GstreamerVideoSource(Observable<VideoSource> source) {
		this.source = source;

		this.videosink = new AppSink("GstVideoComponent");
		videosink.set("emit-signals", true);
		AppSinkListener listener = new AppSinkListener(new InternalConsumer());
		videosink.connect((AppSink.NEW_SAMPLE) listener);
		videosink.connect((AppSink.NEW_PREROLL) listener);
		StringBuilder caps = new StringBuilder("video/x-raw,pixel-aspect-ratio=1/1,");
		// JNA creates ByteBuffer using native byte order, set masks according to that.
		if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN) {
			caps.append("format=BGRx");
		} else {
			caps.append("format=xRGB");
		}
		videosink.setCaps(new Caps(caps.toString()));
		source.addListener((n, o) -> updateURL());
	}

	public void prepare() {
		// Create a PlayBin element and set the AppSink from the Swing component
		// as the video sink.
		playbin = new PlayBin("playbin");
		playbin.setVideoSink(videosink);

		// Create a level component and set it as the audio-filter property
		// on the playbin - this will post audio level messages to the bus -
		// see below how to display them.
		Element level = ElementFactory.make("level", "level");
		playbin.set("audio-filter", level);
	}

	public void start(boolean wait) {
		updateURL();
	}

	public void stop(boolean wait) {
		if (playbin != null) {
			playbin.stop();
		}
	}

	@Override
	public void connect(AudioConsumer consumer) {
		synchronized (audioConsumers) {
			audioConsumers.add(consumer);
		}
	}

	@Override
	public void connect(ImageConsumer consumer) {
		synchronized (imageConsumers) {
			imageConsumers.add(consumer);
		}
	}

	@Override
	public void disconnect(AudioConsumer consumer) {
		synchronized (audioConsumers) {
			audioConsumers.remove(consumer);
		}
	}

	@Override
	public void disconnect(ImageConsumer consumer) {
		synchronized (imageConsumers) {
			imageConsumers.remove(consumer);
		}
	}

	private void updateURL() {
		if (source.get() != null && source.get().url != null && !source.get().url.trim().isEmpty()) {
			try {
				playbin.stop();
				playbin.setURI(new URI(source.get().url));
				playbin.play();
			} catch (URISyntaxException ex) {
				error(ex);
			}
		}
	}

	private class InternalConsumer implements ImageConsumer {

		@Override
		public void accept(ImageData image) {
			synchronized (imageConsumers) {
				imageConsumers.stream().forEach(c -> c.accept(image));
			}
		}

	}
}
