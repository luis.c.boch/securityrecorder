package org.luis.security.recorder.video.api;

import java.awt.image.BufferedImage;

/**
 *
 * @author luis
 */
public class ImageData {

	BufferedImage image;
	int width;
	int height;
	int streamId;
	long duration;

	public ImageData(BufferedImage image, int width, int height, int streamId, long duration) {
		this.image = image;
		this.width = width;
		this.height = height;
		this.streamId = streamId;
		this.duration = duration;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getStreamId() {
		return streamId;
	}

	public void setStreamId(int streamId) {
		this.streamId = streamId;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

}
