package org.luis.security.recorder.video;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import org.luis.security.recorder.main.ExecuteCmd;
import org.luis.security.recorder.main.Logger;
import org.luis.security.recorder.main.VideoSource;
import org.luis.security.recorder.ui.util.FrameUtil;
import org.luis.security.recorder.ui.util.Observable;
import org.luis.security.recorder.ui.util.ThreadUtil;
import org.luis.security.recorder.video.api.ImageConsumer;
import org.luis.security.recorder.video.api.ImageData;

/**
 *
 * @author luis
 */
public class FfmpegStoreConsumer extends Worker implements ImageConsumer, Logger, Observable.Listener<VideoSource> {

	static final String IMAGE_PREFIX_NAME = "image_";
	static final String IMAGE_INDEX_PATTERN_NAME = "%08d";

	Queue<ImageData> imageQueue = new ArrayBlockingQueue<>(5);
	Observable<VideoSource> source;
	Object switchDirLock = new Object();
	String currentDir;
	AtomicInteger atomicInteger = new AtomicInteger();

	final FrameUtil frameUtil = new FrameUtil();

	public FfmpegStoreConsumer(Observable<VideoSource> source) {
		super("humble-video-store", 1, -1);
		this.source = source;
		if (this.source != null) {
			this.source.addListener(this);
		}
	}

	@Override
	void onStart() throws Exception {
		createNewDir();
		log("...prepared");
	}

	@Override
	public void accept(ImageData image) {
		if (running) {
			if (source.get() != null) {
				if (frameUtil.testFPS() <= source.get().fps) {
					frameUtil.tick();
					imageQueue.offer(image);
				}
			}
		}
	}

	@Override
	void doJob() throws Exception {
		ImageData data = null;
		BufferedImage image = null;

		while (running && (source != null && source.get() != null && source.get().store)) {

			/**
			 * Now begin our main loop of taking screen snaps. We're going to
			 * encode and then write out any resulting packets.
			 */
			while (running && (data = imageQueue.poll()) != null) {

				if (!running) {
					continue;
				}

				image = data.getImage();

				if (image == null) {
					continue;
				}

				synchronized (switchDirLock) {
					if (!running || source == null || source.get() == null) {
						continue;
					}

					if (source == null || source.get() == null || !source.get().store) {
						return;
					}

					if (source.get().storeDir == null || source.get().storeDir.trim().isEmpty()) {
						error("No valid store dir: aborting");
						source.get().store = false;
						return;
					}

					new File(source.get().storeDir + "/" + currentDir).mkdirs();
					File file = new File(source.get().storeDir + "/" + currentDir + "/" + IMAGE_PREFIX_NAME + String.format(IMAGE_INDEX_PATTERN_NAME, atomicInteger.incrementAndGet()) + ".jpg");
					ImageIO.write(image, "jpg", file);

				}
			}
			ThreadUtil.sleep(5l);
		}
	}

	@Override
	void onStop() throws Exception {
		flushBuffers();
		if (this.source != null) {
			this.source.removeListener(this);
		}
	}

	private String createDirName() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmm");
		return sdf.format(d);
	}

	public void flushBuffers() {
		if (currentDir == null) {
			return;
		}

		final String dir = currentDir;

		synchronized (switchDirLock) {
			if (running) {
				createNewDir();
			}
		}

		synchronizedAction(() -> {

			String dirName = source.get().storeDir + "/" + dir;
			String path = dirName + "/" + IMAGE_PREFIX_NAME;
			String outputPath = source.get().storeDir + "/" + dir + ".mp4";

			String cmd = "/usr/bin/ffmpeg -r:v 2/1 -i \"" + path + IMAGE_INDEX_PATTERN_NAME + ".jpg\" -r:v 2 -codec:v libx264 -preset veryslow -pix_fmt yuv420p -crf 28 -an \"" + outputPath + "\" && rm -rf \"" + dirName + "\"";
			ExecuteCmd.exec(cmd, (s) -> log(s));
		});
	}

	private void createNewDir() {
		currentDir = createDirName();
		if (source != null && source.get() != null && source.get().store) {
			new File(source.get().storeDir + "/" + currentDir).mkdirs();
		}
		atomicInteger.set(0);
	}

	@Override
	public void notify(VideoSource newValue, VideoSource oldValue) {

		flushBuffers();
	}

	public void swapFile() {
		flushBuffers();
	}

}
