package org.luis.security.recorder.video.api;

import java.util.List;

/**
 *
 * @author luis
 */
public interface StreamSource {

	void connect(ImageConsumer consumer);

	void connect(AudioConsumer consumer);

	void disconnect(ImageConsumer consumer);

	void disconnect(AudioConsumer consumer);
}
