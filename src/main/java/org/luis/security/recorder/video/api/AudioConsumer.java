package org.luis.security.recorder.video.api;

import java.nio.ByteBuffer;

/**
 *
 * @author luis
 */
public interface AudioConsumer extends Consumer<ByteBuffer> {

}
