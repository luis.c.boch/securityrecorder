package org.luis.security.recorder.main;

import java.util.Timer;
import java.util.TimerTask;
import org.luis.security.recorder.ui.util.Observable;
import org.luis.security.recorder.ui.util.Observable.Listener;
import org.luis.security.recorder.video.FfmpegStoreConsumer;
import org.luis.security.recorder.video.GstreamerVideoSource;
import org.luis.security.recorder.video.Worker;
import org.luis.security.recorder.video.api.ImageConsumer;
import org.luis.security.recorder.video.api.ImageData;
import org.luis.security.recorder.video.api.StreamSource;

/**
 *
 * @author luis
 */
public class VideoProcessor implements Logger {

	final Observable<VideoSource> source;
	GstreamerVideoSource videoSource;
	FfmpegStoreConsumer store;

	Timer swapFileTimer;
	final Listener<VideoSource> videoChangeListener;

	public VideoProcessor(Observable<VideoSource> source) {
		this.source = source;
		videoChangeListener = (n, o) -> {
			swapFile();
		};
	}

	public void start() {
		log("Starting...");
		source.addListener(videoChangeListener);

		videoSource = new GstreamerVideoSource(source);
		store = new FfmpegStoreConsumer(source);

		videoSource.prepare();
		videoSource.connect(store);
		videoSource.start(true);
		
		store.start();
		
		configureSwapTimer();
		log("...started");
	}

	public void stop() {
		log("Stopping...");
		source.removeListener(videoChangeListener);

		if (store != null) {
			store.stop();
			store = null;
		}
		if (videoSource != null) {
			videoSource.stop(true);
			videoSource = null;
		}

		stopSwapTimer();

		log("...stopped");
	}

	public StreamSource getStreamSource() {
		return videoSource;
	}

	private void configureSwapTimer() {
		log("scheduled file after: " + source.get().minutesToChangeFile + "m");
		if (swapFileTimer != null) {
			swapFileTimer.cancel();
		}

		swapFileTimer = new Timer("swap-file-timer");
		swapFileTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				swapFile();
			}

		}, source.get().minutesToChangeFile * 60 * 1000);
	}

	private void stopSwapTimer() {
		if (swapFileTimer != null) {
			swapFileTimer.cancel();
			swapFileTimer = null;
		}
	}

	private void swapFile() {
		log("changing file...");
		try {
			store.swapFile();
			// restart this timer task
			swapFileTimer = null;
			configureSwapTimer();
		} catch (Exception ex) {
			error(ex.getMessage());
			error(ex);
		}
	}
	
//	private class ConnectionListener extends Worker implements ImageConsumer{
//		private Long lastImg = System.currentTimeMillis();
//		
//		@Override
//		public void accept(ImageData image) {
//			calculate
//			lastImg = System.currentTimeMillis();
//		}
//
//		@Override
//		void doJob() throws Exception {
//		}
//
//		@Override
//		void onStop() throws Exception {
//		}
//
//		@Override
//		void onStart() throws Exception {
//		}
//		
//	}
}
