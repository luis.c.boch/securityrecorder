package org.luis.security.recorder.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luis
 */
public class ExecuteCmd {

	public static int exec(String cmd, Listener handler) {
		try {
			ProcessBuilder pb = new ProcessBuilder("sh", "-c", cmd);
			pb.inheritIO();

			Process exec = pb.start();
			exec.waitFor();

			read(exec.getErrorStream(), handler);
			read(exec.getInputStream(), handler);

			return exec.exitValue();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private static void read(InputStream errorStream, Listener handler) {
		try (BufferedReader b = new BufferedReader(new InputStreamReader(errorStream))) {
			String line = "";

			while ((line = b.readLine()) != null) {
				handler.offer(line);
			}
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}

	}

	public static interface Listener {

		void offer(String val);
	}
}
