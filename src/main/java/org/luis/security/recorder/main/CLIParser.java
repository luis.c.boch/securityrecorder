package org.luis.security.recorder.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import javax.swing.JFrame;
import org.luis.security.recorder.ui.Window;
import org.luis.security.recorder.video.Worker;

/**
 *
 * @author luis
 */
public class CLIParser implements Logger {

	Window window;

	@SuppressWarnings("NestedAssignment")
	public void read() {
		try (InputStreamReader isr = new InputStreamReader(System.in);
				BufferedReader reader = new BufferedReader(isr)) {

			String line = null;

			while ((line = reader.readLine()) != null) {
				if (line == null) {
					continue;
				}

				if (line.startsWith("help")) {
					printHelp();
				}

				if (line.startsWith("gui")) {
					showScreen(false);
				}

				if (line.equals("quit")) {
					VideoManager.getInstance().stop();
					System.exit(0);
				}
			}
		} catch (IOException ex) {
			log(ex);
		}
	}

	public void showScreen(boolean exitOnClose) {
		if (window == null) {
			window = new Window();
			window.setDefaultCloseOperation(
					exitOnClose
							? JFrame.EXIT_ON_CLOSE : JFrame.DISPOSE_ON_CLOSE);
		}
		window.setVisible(true);
	}

	public void printUsage() {
		PrintStream out = System.out;
		out.println("Usage: \n");
		out.println("\t--help -h\t\t\t\t: Prints this help");
		out.println("\t--theme:light\t\t\t: Use light theme");
		out.println("\t--theme:dark (default)\t\t: Use dark theme");
		out.println("\t--gui -g:\t\t\t Start with GUI");
	}

	public void printHelp() {
		PrintStream out = System.out;
		out.println("Interactive: \n");
		out.println("\thelp\t\t\t: Prints this help");
		out.println("\tgui\t\t\t: Start GUI");
		out.println("\tquit\t\t\t: Stop application");
	}

}
