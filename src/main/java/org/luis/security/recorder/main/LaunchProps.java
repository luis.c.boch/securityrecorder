package org.luis.security.recorder.main;

import org.luis.security.recorder.ui.util.Observable;

/**
 *
 * @author luis
 */
public class LaunchProps {

	public static Observable<Boolean> useLightTheme = new Observable<>(false);
	public static final Observable<Boolean> showUI = new Observable<>(false);
	public static final Observable<Boolean> bindLog = new Observable<>(true);

}
