package org.luis.security.recorder.main;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.luis.security.recorder.ui.FileManager;

/**
 *
 * @author luis
 */
public interface Logger {

	static final SimpleDateFormat _dateFormat = new SimpleDateFormat("yyMMdd HH:mm:ss.SSS");
	static final PrintWriter out = FileManager.getInstance().createLogger("root");

	default void log(Object... values) {
		if (values == null || values.length == 0) {
			return;
		}

		for (Object ob : values) {
			if (ob instanceof Throwable) {
				((Throwable) ob).printStackTrace(out);
				if (LaunchProps.bindLog.get()) {
					((Throwable) ob).printStackTrace(System.out);
				}
			} else {
				synchronized (_dateFormat) {
					out.println("[" + _dateFormat.format(new Date()) + "|" + getClass().getSimpleName() + "] " + String.valueOf(ob));
					out.flush();

					if (LaunchProps.bindLog.get()) {
						System.out.println("[" + _dateFormat.format(new Date()) + "|" + getClass().getSimpleName() + "] " + String.valueOf(ob));
						System.out.flush();
					}
				}
			}
		}
	}

	default void error(String val) {
		log("ERROR: " + val);
	}

	default void error(Throwable ex) {
		log(ex);
	}
}
