package org.luis.security.recorder.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.luis.security.recorder.ui.FileManager;
import org.luis.security.recorder.ui.util.Observable;
import org.luis.security.recorder.ui.util.ScheduledUpdate;
import org.luis.security.recorder.video.Worker;

/**
 *
 * @author luis
 */
public class VideoManager implements Observable.Listener<WindowPreferences>, Logger {

	private static VideoManager instance;

	public final Observable<WindowPreferences> preferences = new Observable<>();
	public final FileManager fileManager = FileManager.getInstance();
	public final ScheduledUpdate actionManager = ScheduledUpdate.getInstance();
	public final List<Observable<VideoSource>> sources = new ArrayList<>();
	private final Map<Integer, VideoProcessor> processors = new TreeMap<>();
	private boolean started = false;
	private final Object startStopLock = new Object();

	private VideoManager() {
	}

	public void start() {
		if (started) {
			return;
		}

		log("Starting...");

		synchronized (startStopLock) {
			if (started) {
				return;
			}
			loadPrefsFromFile();
			preferences.addListener(this);
			startProcessors();
			started = true;
		}
		log("...started");

	}

	public synchronized static VideoManager getInstance() {
		if (instance == null) {
			instance = new VideoManager();
		}
		return instance;
	}

	private void saveFilePreferences() {
		actionManager.schedule("save-preferences", 200l, () -> {
			log("Saving window preferences");
			fileManager.save("prefs", preferences.get());
		});
	}

	private void loadPrefsFromFile() {
		log("loading preferences...");
		WindowPreferences prefs = fileManager.load(WindowPreferences.class, "prefs");

		if (prefs == null) {
			prefs = new WindowPreferences();
			prefs.zoom = prefs.zoomValues[1];
		}

		preferences.set(prefs);

		loadVideoSources();
	}

	private void loadVideoSources() {
		log("loading " + preferences.get().ids.size() + " sources");
		preferences.get().ids.forEach(id -> {
			log("loading source: " + id);
			final VideoSource source = loadVideoSource(id);
			if (source != null) {
				Observable<VideoSource> obs = new Observable<>(source);
				obs.addListener((n, o) -> saveVideoSource(n));
				sources.add(obs);
				preferences.get().lastId = id > preferences.get().lastId ? id : preferences.get().lastId;
			}
		});

		UID.startAt(preferences.get().lastId);
	}

	public void addVideoSource(VideoSource vs) {
		Observable<VideoSource> obs = new Observable<>(vs);
		addVideoSource(obs);
	}

	public void addVideoSource(Observable<VideoSource> vs) {
		log("adding video source :" + vs.get().id);
		vs.addListener((n, o) -> saveVideoSource(n));
		sources.add(vs);
		startProcessorFor(vs);
		preferences.get().ids.add(vs.get().id);
		preferences.forceNotify();
		saveVideoSource(vs.get());
	}

	private void saveVideoSource(VideoSource source) {
		actionManager.schedule("save-source-" + source.id, 200l, () -> {
			if (source.markedToRemove) {
				log("removing video source :" + source.id);
				preferences.get().ids.removeIf((id) -> source.id == id);
				preferences.forceNotify();
				stopProcessor(source.id);
				fileManager.delete("sources/video." + source.id);
			} else {
				log("saving video source :" + source.id);
				fileManager.save("sources/video." + source.id, source);
			}
		});

	}

	private VideoSource loadVideoSource(int id) {
		return fileManager.load(VideoSource.class, "sources/video." + id);
	}

	public void stop() {
		if (!started) {
			return;
		}

		log("stoping...");
		synchronized (startStopLock) {
			preferences.removeListener(this);
			processors.entrySet().forEach(e -> e.getValue().stop());
			processors.clear();
			sources.clear();
			started = false;
			log("...done");
		}
		
		Worker.waitForExit();
	}

	@Override
	public void notify(WindowPreferences newValue, WindowPreferences oldValue) {
		saveFilePreferences();
	}

	private void startProcessors() {
		log("starting processors...");
		sources.forEach(o -> {
			startProcessorFor(o);
		});
		log("...done");
	}

	public VideoProcessor getVideoProcessor(VideoSource s) {
		return processors.get(s.id);
	}

	private void startProcessorFor(Observable<VideoSource> o) {
		VideoProcessor p = new VideoProcessor(o);
		processors.put(o.get().id, p);
		p.start();
	}

	private void stopProcessor(int id) {
		log("stopping processr " + id);
		VideoProcessor processor = processors.get(id);
		if (processor != null) {
			processor.stop();
		}
	}

}
