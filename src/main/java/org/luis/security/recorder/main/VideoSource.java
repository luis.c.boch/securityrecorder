package org.luis.security.recorder.main;

import com.google.gson.annotations.Expose;

/**
 *
 * @author luis
 */
public class VideoSource {

	public final int id = UID.next();

	public String url;
	public boolean store;
	public String storeDir;
	public Integer minutesToChangeFile = 15;
	public Integer fps = 2;
	public boolean videoPreview = true;
	public boolean audioPreview = true;

	@Expose(serialize = false, deserialize = false)
	public boolean markedToRemove = false;

}
