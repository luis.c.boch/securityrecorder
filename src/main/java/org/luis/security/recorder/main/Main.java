package org.luis.security.recorder.main;

import java.net.URISyntaxException;

/**
 * A Simple videotest example.
 */
public class Main {

	public static void main(String[] args) throws URISyntaxException {

		readArgs(args);
		new Thread(() -> {
			VideoManager.getInstance().start();
		}, "startup").start();

		CLIParser cliParser = new CLIParser();

		if (LaunchProps.showUI.get()) {
			cliParser.showScreen(true);

		} else {
			cliParser.printHelp();
			cliParser.read();
		}

	}

	private static void readArgs(String[] args) {
		LaunchProps.showUI.set(true);
		if (args != null) {
			for (String arg : args) {
				if (arg == null) {
					continue;
				}

				if (arg.equals("--theme:light")) {
					LaunchProps.useLightTheme.set(true);
				}

				if (arg.equals("--interactive") || arg.equals("-i")) {
					LaunchProps.showUI.set(false);
					LaunchProps.bindLog.set(false);
				}
			}
		}
	}
}
