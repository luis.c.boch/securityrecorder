package org.luis.security.recorder.main;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luis
 */
public class WindowPreferences {

	public final int[] zoomValues = new int[]{220, 300, 400, 620, 940, 1200, 1600, 1900};
	public int zoom;
	public List<Integer> ids = new ArrayList<>();
	public int screenWidth;
	public int screenHeight;
	public int lastId;
	public boolean alwaysRenderVidControls;

}
