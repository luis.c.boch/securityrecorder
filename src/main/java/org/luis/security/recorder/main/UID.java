package org.luis.security.recorder.main;

/**
 *
 * @author luis
 */
public class UID {

	private static final Object lock = new Object();
	private static int id = 0;

	public static int next() {
		synchronized (lock) {
			id++;
			return id;
		}
	}

	public static void startAt(int id) {
		synchronized (lock) {
			UID.id = id;
		}
	}
}
