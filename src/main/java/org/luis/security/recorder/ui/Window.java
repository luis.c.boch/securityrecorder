package org.luis.security.recorder.ui;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import java.awt.BorderLayout;
import org.luis.security.recorder.main.WindowPreferences;
import org.luis.security.recorder.main.VideoSource;
import org.luis.security.recorder.ui.util.ScheduledUpdate;
import org.luis.security.recorder.ui.util.WrapLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;
import javax.swing.SwingUtilities;
import org.luis.security.recorder.main.LaunchProps;
import org.luis.security.recorder.main.Logger;
import org.luis.security.recorder.main.VideoManager;
import org.luis.security.recorder.ui.util.Observable;

/**
 *
 * @author luis
 */
public class Window extends javax.swing.JFrame implements Logger {

	Dimension previewSize;
	private int currentIndex = 1;

	private VideoManager manager;
	private VideoPreview hightLight;
	private boolean starting;

	public Window() {

		if (!LaunchProps.useLightTheme.get()) {
			FlatDarkLaf.install();
		} else {
			FlatLightLaf.install();
		}
		starting = true;
		initComponents();
		setIconImage(FileManager.getInstance().getRawIcon("icon.png").getImage());

	}

	private void openWindow() {
		log("Opening window");
		setupIcons();
		readManager();
		applyPreviewSize();
		manager.preferences.addListener((n, o) -> applyPrefesUpdate());
		starting = false;
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        loading = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        container = new javax.swing.JPanel();
        panelWindowControls = new javax.swing.JPanel();
        chkShowVideoControl = new javax.swing.JToggleButton();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 15), new java.awt.Dimension(15, 15), new java.awt.Dimension(32767, 0));
        btnZoomIn = new javax.swing.JButton();
        btnZoomOut = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(15, 15), new java.awt.Dimension(15, 15), new java.awt.Dimension(32767, 0));
        btnAdd = new javax.swing.JButton();
        btnInfo = new javax.swing.JButton();
        videoListContainer = new javax.swing.JScrollPane();
        panelVideoList = new javax.swing.JPanel();

        loading.setLayout(new java.awt.GridBagLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons/loading.gif"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        loading.add(jLabel1, gridBagConstraints);

        jLabel2.setText("Iniciando...");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        loading.add(jLabel2, gridBagConstraints);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(300, 300));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        panelWindowControls.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        chkShowVideoControl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons/control_16.png"))); // NOI18N
        chkShowVideoControl.setToolTipText("Show video controls");
        chkShowVideoControl.setMinimumSize(new java.awt.Dimension(32, 32));
        chkShowVideoControl.setPreferredSize(new java.awt.Dimension(32, 32));
        chkShowVideoControl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkShowVideoControlActionPerformed(evt);
            }
        });
        panelWindowControls.add(chkShowVideoControl);
        panelWindowControls.add(filler2);

        btnZoomIn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons/zoom_in_16.png"))); // NOI18N
        btnZoomIn.setMaximumSize(new java.awt.Dimension(32, 32));
        btnZoomIn.setMinimumSize(new java.awt.Dimension(32, 32));
        btnZoomIn.setPreferredSize(new java.awt.Dimension(32, 32));
        btnZoomIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnZoomInActionPerformed(evt);
            }
        });
        panelWindowControls.add(btnZoomIn);

        btnZoomOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons/zoom_out_16.png"))); // NOI18N
        btnZoomOut.setMaximumSize(new java.awt.Dimension(32, 32));
        btnZoomOut.setMinimumSize(new java.awt.Dimension(32, 32));
        btnZoomOut.setPreferredSize(new java.awt.Dimension(32, 32));
        btnZoomOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnZoomOutActionPerformed(evt);
            }
        });
        panelWindowControls.add(btnZoomOut);
        panelWindowControls.add(filler1);

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons/add_16.png"))); // NOI18N
        btnAdd.setMaximumSize(new java.awt.Dimension(32, 32));
        btnAdd.setMinimumSize(new java.awt.Dimension(32, 32));
        btnAdd.setPreferredSize(new java.awt.Dimension(32, 32));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        panelWindowControls.add(btnAdd);

        btnInfo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/icons/info_16.png"))); // NOI18N
        btnInfo.setMaximumSize(new java.awt.Dimension(32, 32));
        btnInfo.setMinimumSize(new java.awt.Dimension(32, 32));
        btnInfo.setPreferredSize(new java.awt.Dimension(32, 32));
        btnInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInfoActionPerformed(evt);
            }
        });
        panelWindowControls.add(btnInfo);

        videoListContainer.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        javax.swing.GroupLayout panelVideoListLayout = new javax.swing.GroupLayout(panelVideoList);
        panelVideoList.setLayout(panelVideoListLayout);
        panelVideoListLayout.setHorizontalGroup(
            panelVideoListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 584, Short.MAX_VALUE)
        );
        panelVideoListLayout.setVerticalGroup(
            panelVideoListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 331, Short.MAX_VALUE)
        );

        panelVideoList.setLayout(new WrapLayout(WrapLayout.LEADING));

        videoListContainer.setViewportView(panelVideoList);

        videoListContainer.getVerticalScrollBar().setUnitIncrement(20);

        javax.swing.GroupLayout containerLayout = new javax.swing.GroupLayout(container);
        container.setLayout(containerLayout);
        containerLayout.setHorizontalGroup(
            containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 505, Short.MAX_VALUE)
            .addGroup(containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(containerLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(panelWindowControls, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(videoListContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 493, Short.MAX_VALUE))
                    .addContainerGap()))
        );
        containerLayout.setVerticalGroup(
            containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 393, Short.MAX_VALUE)
            .addGroup(containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(containerLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelWindowControls, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(videoListContainer)
                    .addContainerGap()))
        );

        getContentPane().add(container, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
		DialogViewConfiguration dialog = new DialogViewConfiguration(this);
		dialog.setVisible(true);
		if (dialog.accepted) {
			log("adding new video");
			manager.addVideoSource(dialog.source);
			addVideoSource(dialog.source);
			updateVideoList();
		}
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnZoomInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnZoomInActionPerformed

		if (currentIndex >= (manager.preferences.get().zoomValues.length - 1)) {
			return;
		}
		currentIndex++;
		applyPreviewSize();
		/**
		 * Inside apply we change zoom preferece. Then we need to save it
		 */
		manager.preferences.forceNotify();
    }//GEN-LAST:event_btnZoomInActionPerformed

    private void btnZoomOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnZoomOutActionPerformed

		if (currentIndex <= 0) {
			return;
		}

		currentIndex--;
		applyPreviewSize();
		/**
		 * Inside apply we change zoom preferece. Then we need to save it
		 */
		manager.preferences.forceNotify();
    }//GEN-LAST:event_btnZoomOutActionPerformed

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
		if (starting) {
			return;
		}
		updateLayoutConf();

		if (manager == null) {
			// starting...
			return;
		}

		if (manager.preferences.get() == null) {
			return;
		}

		manager.preferences.get().screenWidth = getSize().width;
		manager.preferences.get().screenHeight = getSize().height;
		manager.preferences.forceNotify();

    }//GEN-LAST:event_formComponentResized

    private void chkShowVideoControlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkShowVideoControlActionPerformed
		manager.preferences.get().alwaysRenderVidControls = chkShowVideoControl.isSelected();
		manager.preferences.forceNotify();

		eachVideoPreview((vp) -> {
			vp.setShowControls(chkShowVideoControl.isSelected());
		});

		applyPreviewSize();
    }//GEN-LAST:event_chkShowVideoControlActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
		if (LaunchProps.showUI.get()) {
			// We started with UI, then, when close, stop
			manager.stop();
		}

		log("Closing window");

    }//GEN-LAST:event_formWindowClosing

    private void btnInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInfoActionPerformed
		new AboutDialog(this).setVisible(true);
    }//GEN-LAST:event_btnInfoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnInfo;
    private javax.swing.JButton btnZoomIn;
    private javax.swing.JButton btnZoomOut;
    private javax.swing.JToggleButton chkShowVideoControl;
    private javax.swing.JPanel container;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel loading;
    private javax.swing.JPanel panelVideoList;
    private javax.swing.JPanel panelWindowControls;
    private javax.swing.JScrollPane videoListContainer;
    // End of variables declaration//GEN-END:variables

	private void applyPreviewSize() {
		log("applying preview size");
		int width = manager.preferences.get().zoomValues[currentIndex];

		manager.preferences.get().zoom = width;
		float aspect = (manager.preferences.get().alwaysRenderVidControls ? 3f : 2.5f) / 4f;
		int height = (int) (((float) width) * aspect);
		previewSize = new Dimension(width, height);

		eachVideoPreview((vp) -> {
			if (hightLight != null && vp.source.get().id == hightLight.source.get().id) {
				return;
			}

			vp.setPreferredSize(previewSize);
			vp.setSize(previewSize);
		});
		updateHightlightSize();
		panelVideoList.revalidate();
	}

	private void updateVideoList() {
		log("scheduled update video list");
		ScheduledUpdate.getInstance().schedule("video-list-update", 200l, () -> {
			SwingUtilities.invokeLater(() -> {
				panelVideoList.revalidate();
				panelVideoList.repaint();
				log("video list updated");
			});
		});
	}

	private void readManager() {

		log("reading preferences");

		WindowPreferences prefs = manager.preferences.get();
		reloadZoomConf();

		for (Observable<VideoSource> source : manager.sources) {
			addVideoSource(source);
		}

		if (prefs.screenWidth > 0 && prefs.screenHeight > 0) {
			log("windowsize: (" + prefs.screenWidth + ", " + prefs.screenHeight + ")");
			setSize(new Dimension(prefs.screenWidth, prefs.screenHeight));
			setLocationRelativeTo(null);
		}

		applyWindowPreferences();
		updateLayoutConf();
		updateVideoList();
	}

	private void addVideoSource(Observable<VideoSource> source) {

		VideoPreview vp = new VideoPreview(this, source);

		vp.setPreferredSize(previewSize);
		vp.setSize(previewSize);
		vp.setShowControls(manager.preferences.get().alwaysRenderVidControls);

		source.addListener((n, o) -> {
			if (n.markedToRemove) {
				panelVideoList.remove(vp);
			}
		});

		panelVideoList.add(vp);
	}

	private void reloadZoomConf() {
		int[] zoomValues = manager.preferences.get().zoomValues;

		for (int i = 0; i < zoomValues.length; i++) {
			if (manager.preferences.get().zoom == zoomValues[i]) {
				currentIndex = i;
			}
		}

		applyPreviewSize();
	}

	private void updateLayoutConf() {
		Dimension panelSize = panelVideoList.getSize();
		panelSize.width = getSize().width;
		panelVideoList.setMaximumSize(new Dimension(getSize().width, panelVideoList.getMaximumSize().height));

		updateHightlightSize();
	}

	private void updateHightlightSize() {
		if (hightLight != null) {
			Dimension size = panelVideoList.getSize();

			hightLight.setSize(
					new Dimension(
							(int) (((float) size.width) * 0.9f),
							(int) (((float) size.height) * 0.9f)));
			hightLight.setPreferredSize(hightLight.getSize());
		}
	}

	private Stream<VideoPreview> getVideoPreviewStream() {
		List<VideoPreview> list = new ArrayList<>();
		for (Component vp : panelVideoList.getComponents()) {
			if (!(vp instanceof VideoPreview)) {
				continue;
			}
			list.add((VideoPreview) vp);
		}

		return list.stream();
	}

	private void eachVideoPreview(Consumer<VideoPreview> cons) {
		getVideoPreviewStream().forEach((vp) -> {
			try {
				cons.accept(vp);
			} catch (Exception ex) {
				error(ex);
			}
		});
	}

	private void setupIcons() {
		FileManager manager = FileManager.getInstance();

		btnAdd.setIcon(manager.getIcon("add"));
		btnZoomIn.setIcon(manager.getIcon("zoom_in"));
		btnZoomOut.setIcon(manager.getIcon("zoom_out"));
		chkShowVideoControl.setIcon(manager.getIcon("control"));
		btnInfo.setIcon(manager.getIcon("info"));
	}

	private void applyWindowPreferences() {
		chkShowVideoControl.setSelected(manager.preferences.get().alwaysRenderVidControls);
	}

	private void applyPrefesUpdate() {
		manager.actionManager.schedule("apply-preferences", 200l, () -> {
			applyWindowPreferences();
		});
	}

	void setToogleFullPreview(VideoPreview p) {
		if (hightLight == null) {
			getVideoPreviewStream().filter(f -> p.source.get().id != f.source.get().id)
					.forEach(f -> f.setVisible(false));
			p.setVisible(true);
			hightLight = p;
			updateHightlightSize();
		} else {
			hightLight = null;
			getVideoPreviewStream()
					.forEach(f -> f.setVisible(true));
			applyPreviewSize();
		}
	}

	@Override
	public void setVisible(boolean b) {
		if (b) {
			SwingUtilities.invokeLater(() -> {
				if (manager == null) {
					setResizable(false);
					setEnabled(false);
					setFocusable(false);
					setLocationRelativeTo(null);

					getContentPane().remove(container);
					getContentPane().add(loading, BorderLayout.CENTER);
					new Thread(() -> {
						manager = VideoManager.getInstance();
						manager.start();
						SwingUtilities.invokeLater(() -> {
							getContentPane().remove(loading);
							getContentPane().add(container, BorderLayout.CENTER);
							setResizable(true);
							setEnabled(true);
							openWindow();
						});
					}).start();
				}
			});
			super.setVisible(b);
		} else {
			super.setVisible(b);
		}
	}

}
