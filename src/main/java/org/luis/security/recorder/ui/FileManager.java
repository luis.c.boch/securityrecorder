package org.luis.security.recorder.ui;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import org.apache.commons.io.IOUtils;
import org.luis.security.recorder.main.LaunchProps;
import org.luis.security.recorder.main.Logger;

/**
 *
 * @author luis
 */
public class FileManager implements Logger {

	private static FileManager instance;

	String rootPath = "";

	public static synchronized FileManager getInstance() {

		if (instance == null) {
			instance = new FileManager();
		}

		return instance;
	}

	private FileManager() {
		rootPath = System.getProperty("root-preferences-dir");
		if (rootPath == null || rootPath.trim().isEmpty()) {
			rootPath = System.getProperty("user.home") + "/.sec.rec";
		}

		if (rootPath.endsWith("/")) {
			rootPath = rootPath.substring(0, rootPath.length() - 1);
		}

	}

	private void ensureFileExtruture() {
		ensureDir(rootPath);
		ensureDir(rootPath + "/sources");
		ensureDir(rootPath + "/log");
	}

	public boolean ensureDir(String dir) {
		File file = new File(dir);
		if (file.exists()) {
			if (!file.isDirectory()) {
				error("File exists and we cant create dir with this path: " + dir);
				return false;
			}
		}

		file.mkdirs();
		return true;
	}

	public void save(String fileLoc, Object source) {
		File file = new File(rootPath + "/" + fileLoc);
		ensureFileExtruture();

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException ex) {
				error("creating file: " + ex.getMessage());
				return;
			}
		}

		try (FileOutputStream os = new FileOutputStream(file)) {
			IOUtils.write(new Gson().toJson(source), os, Charset.forName("UTF-8"));
		} catch (IOException ioex) {
			error(ioex);
		}
	}

	public void delete(String fileLoc) {
		ensureFileExtruture();
		File file = new File(rootPath + "/" + fileLoc);
		file.delete();
	}

	public <A> A load(Class<A> type, String fileLoc) {
		File file = new File(rootPath + "/" + fileLoc);
		try (FileInputStream os = new FileInputStream(file)) {
			return new Gson().fromJson(new String(IOUtils.toByteArray(os), "UTF-8"), type);
		} catch (FileNotFoundException ioex) {
			return null;
		} catch (IOException ioex) {
			error(ioex);
			return null;
		}
	}

	public ImageIcon getRawIcon(String name) {
		URL res = getClass().getResource("/images/icons/" + name);
		return new ImageIcon(res);
	}

	public ImageIcon getIcon(String name) {
		String lookingPath = name + "_" + (LaunchProps.useLightTheme.get() ? "" : "l_") + "16.png";
		URL res = getClass().getResource("/images/icons/" + lookingPath);

		if (res == null) {
			log("ICON not found: " + name + " at :" + lookingPath);
			return null;
		}

		return new javax.swing.ImageIcon(res);
	}

	private String createFileName() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmm");
		return sdf.format(d) + ".log";
	}

	public PrintWriter createLogger(String name) {
		ensureFileExtruture();
		try {
			String path = rootPath + "/log/" + name + "_" + createFileName();
			File file = new File(path);

			file.createNewFile();
			return new PrintWriter(file);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
