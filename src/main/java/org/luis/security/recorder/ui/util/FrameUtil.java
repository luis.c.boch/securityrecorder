package org.luis.security.recorder.ui.util;

import org.luis.security.recorder.main.Logger;

/**
 *
 * @author luis
 */
public class FrameUtil implements Logger {

	long last = System.currentTimeMillis();
	long now;
	int currentFps = 0;

	public int testFPS() {
		now = System.currentTimeMillis();
		long diff = now - last;
		return (int) (1000d / ((double) diff));
	}

	public void tick() {
		last = now;
	}
}
