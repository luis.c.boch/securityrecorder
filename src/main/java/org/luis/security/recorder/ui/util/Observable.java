package org.luis.security.recorder.ui.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author luis
 * @param <T>
 */
public class Observable<T> {

	private T value = null;
	private T oldValue = null;

	private final Set<Listener<T>> listeners = new HashSet<>();

	public Observable() {
	}

	public Observable(T value) {
		this.value = value;
	}

	public void set(T value, boolean notify) {
		setValue(value, notify);
	}

	public boolean has(Listener<T> listener) {
		return listeners.contains(listener);
	}

	public void setValue(T value, boolean notify) {
		oldValue = this.value;
		this.value = value;

		if (!notify) {
			return;
		}

		if (!Objects.equals(this.value, oldValue)) {
			this.notify(this.value, oldValue);
		}
	}

	public void setValue(T value) {
		setValue(value, true);
	}

	public void set(T value) {
		setValue(value, true);
	}

	public T getValue() {
		return value;
	}

	public T get() {
		return value;
	}

	public void watch(Listener<T> listener) {
		addListener(listener);
	}

	public void addListener(Listener<T> listener) {
		synchronized (listeners) {
			listeners.add(listener);
		}
	}

	public void removeListener(Listener<T> listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}

	private void notify(T value, T oldValue) {

		synchronized (listeners) {
			new ArrayList<>(listeners).forEach((l) -> l.notify(value, oldValue));
		}

		this.oldValue = null;
	}

	public void forceNotify() {
		notify(value, oldValue);
	}

	@FunctionalInterface
	public interface Listener<T> {

		void notify(T newValue, T oldValue);
	}
}
