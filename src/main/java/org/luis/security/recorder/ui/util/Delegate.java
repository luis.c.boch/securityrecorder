package org.luis.security.recorder.ui.util;

/**
 *
 * @author luis
 * @param <T>
 */
@FunctionalInterface
public interface Delegate<T> {

	T resolve();
}
