/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luis.security.recorder.ui.util;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Window;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 *
 * @author luis
 */
public class JDialogEscape extends javax.swing.JDialog {

	public JDialogEscape() {
	}

	public JDialogEscape(Frame owner) {
		super(owner);
		init();
	}

	public JDialogEscape(Frame owner, boolean modal) {
		super(owner, modal);
		init();
	}

	public JDialogEscape(Frame owner, String title) {
		super(owner, title);
		init();
	}

	public JDialogEscape(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
		init();
	}

	public JDialogEscape(Frame owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
		init();
	}

	public JDialogEscape(Dialog owner) {
		super(owner);
		init();
	}

	public JDialogEscape(Dialog owner, boolean modal) {
		super(owner, modal);
		init();
	}

	public JDialogEscape(Dialog owner, String title) {
		super(owner, title);
		init();
	}

	public JDialogEscape(Dialog owner, String title, boolean modal) {
		super(owner, title, modal);
		init();
	}

	public JDialogEscape(Dialog owner, String title, boolean modal, GraphicsConfiguration gc) {
		super(owner, title, modal, gc);
		init();
	}

	public JDialogEscape(Window owner) {
		super(owner);
		init();
	}

	public JDialogEscape(Window owner, Dialog.ModalityType modalityType) {
		super(owner, modalityType);
		init();
	}

	public JDialogEscape(Window owner, String title) {
		super(owner, title);
		init();
	}

	public JDialogEscape(Window owner, String title, Dialog.ModalityType modalityType) {
		super(owner, title, modalityType);
		init();
	}

	public JDialogEscape(Window owner, String title, Dialog.ModalityType modalityType, GraphicsConfiguration gc) {
		super(owner, title, modalityType, gc);
		init();
	}

	private void init() {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
					setVisible(false);
				}
			}
		});

	}

}

// Variables declaration - do not modify                     
    // End of variables declaration                   
