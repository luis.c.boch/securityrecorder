package org.luis.security.recorder.ui.util;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import org.luis.security.recorder.main.Logger;

/**
 *
 * @author luis
 */
public class ScheduledUpdate implements Logger {

	private static ScheduledUpdate _INSTANCE;

	private boolean running;
	private Thread worker;
	private final Set<Conf> actions = new HashSet<>();

	private ScheduledUpdate() {
		worker = new Thread(new BgWorker(), "security-scheduled-worker");
		running = true;
		worker.start();
	}

	public static synchronized ScheduledUpdate getInstance() {
		if (_INSTANCE == null) {
			_INSTANCE = new ScheduledUpdate();
		}
		return _INSTANCE;
	}

	public void schedule(String name, Long waitForStart, Runnable action) {
		synchronized (actions) {

			for (Conf c : actions) {
				if (c.name.equals(name)) {
					c.lastTick = System.currentTimeMillis();
					c.action = action;
					return;
				}
			}

			actions.add(new Conf(name, waitForStart, action, System.currentTimeMillis()));
		}

	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	private class Conf {

		public Conf(String name, Long waitMs, Runnable action, Long lastTick) {
			this.name = name;
			this.waitMs = waitMs;
			this.action = action;
			this.lastTick = lastTick;
		}

		String name;
		Long waitMs;
		Runnable action;
		Long lastTick;

		@Override
		public int hashCode() {
			int hash = 5;
			hash = 23 * hash + Objects.hashCode(this.name);
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final Conf other = (Conf) obj;
			if (!Objects.equals(this.name, other.name)) {
				return false;
			}
			return true;
		}

	}

	private class BgWorker implements Runnable {

		@Override
		public void run() {
			while (running) {
				synchronized (actions) {
					Set<Conf> toRemove = new HashSet<>();
					for (Conf conf : actions) {
						if ((System.currentTimeMillis() - conf.lastTick) > conf.waitMs) {
							try {
								conf.action.run();
							} catch (Throwable ex) {
								error(ex);
							}
							toRemove.add(conf);
						}
					}

					actions.removeAll(toRemove);
				}

				try {
					Thread.sleep(100l);
				} catch (InterruptedException ex) {
				}
			}

		}

	}
}
