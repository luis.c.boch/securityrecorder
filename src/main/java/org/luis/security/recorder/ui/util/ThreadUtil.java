package org.luis.security.recorder.ui.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luis
 */
public class ThreadUtil {

	public static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException ex) {
			Logger.getLogger(ThreadUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static boolean waitTo(long timeToWait, Delegate<Boolean> finished) {
		return waitTo(timeToWait, finished, null, true);
	}

	/**
	 * Wait for given timeToWait, When timed out, will return false. Action will
	 * execute when finished returns true or abortOnTimeout is false
	 *
	 * @param timeToWait
	 * @param finished
	 * @param rn
	 * @param abortOnTimeout
	 * @return
	 */
	public static boolean waitTo(long timeToWait, Delegate<Boolean> finished, Runnable rn, boolean abortOnTimeout) {
		long startWaiting = System.currentTimeMillis();
		boolean timedout = false;

		while (!finished.resolve() && !timedout) {
			long waiting = System.currentTimeMillis() - startWaiting;

			if (waiting > timeToWait) {
				timedout = true;
			}

			sleep(200l);
		}

		if (rn != null && (timedout || !abortOnTimeout)) {
			rn.run();
		}

		return !timedout;

	}

	public static StackTraceElement getStack(int idx) {
		StackTraceElement[] stack
				= Thread.getAllStackTraces().get(Thread.currentThread());
		return stack[idx];
	}

	public static String getCallerClassMethod() {
		StackTraceElement stack = getStack(5);
		return stack.getClassName() + ":" + stack.getMethodName() + ":" + stack.getLineNumber();
	}

}
