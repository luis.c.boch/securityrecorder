package org.luis.security.recorder;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.luis.security.recorder.ui.util.ThreadUtil;

/**
 *
 * @author luis
 */
public class ThreadUtilTest {

	@Test
	public void testarWaitTimeOut() {
		long timeToWait = 1500l;
		long current = System.currentTimeMillis();
		ThreadUtil.waitTo(timeToWait, () -> false);
		assertTrue((System.currentTimeMillis() - current) > timeToWait);
	}

	@Test
	public void testarWaitSuccess() {
		long timeToWait = 01500l;
		long current = System.currentTimeMillis();
		ThreadUtil.waitTo(timeToWait, () -> true);
		assertFalse((System.currentTimeMillis() - current) > timeToWait);
	}
}
